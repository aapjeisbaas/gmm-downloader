#/bin/bash

# 1 param (media destination)
cd $1

for id in $(curl -s http://vod.graspop.be/nl |grep 'href="/nl/' | sed 's/.*href="\/nl/https:\/\/videocenter.proximusmwc.be\/festivals/g; s/">//g') ; do
  media_url=$(curl -s $id | grep "data-config=" | sed 's/.*{&quot;hls&quot;:&quot;https:/https:/g; s/&quot.*//g; s/\\//g' | grep 'fullconcert')
  media_name=$(echo $media_url | sed 's/.*\/festivals\///g; s/\/.*//g')
  youtube-dl --no-post-overwrites --no-overwrites -f best --no-check-certificate $media_url -o $media_name.mp4
done
